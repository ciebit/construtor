<?php
/*
==================================
------------- CIEBIT -------------
==================================
*/
namespace Ciebit;

class CTInterface
{
	public $blocos = array();
	public $css       = '';
	public $css_linha = null;
	public $css_comando = '';
	public $descricao = '';
	public $imp       = array();
	public $js        = '';
	public $js_linha  = null;
	public $palavras  = '';
	public $redes     = '';
	public $titulo    = '';

	public function bloco( $id )
	{
		if( !isset( $this->blocos[ $id ] ) ) return '';
		if( !is_file( $this->blocos[ $id ][0] ) ) throw new Exception( "Arquivo de bloco {$this->blocos[ $id ][0]} não encontrado", 1 );

		$blocoDados = $this->blocos[ $id ][1];

		include $this->blocos[ $id ][ 0 ];

		return $this;
	}
	public function css()
	{
		echo $this->css;
		$this->cssLinha();
		return $this;
	}
	public function cssComando()
	{
		if($this->css_comando) echo "\t\t<style type=\"text/css\">{$this->css_comando}</style>\n";
		return $this;
	}
	public function cssLinha()
	{
		if($this->css_linha) echo "\t\t<style type=\"text/css\">{$this->css_linha}</style>\n";
		return $this;
	}
	public function descricao()
	{
		if($this->descricao) echo "\t\t<meta name=\"description\" content=\"{$this->descricao}\">\n";
		return $this;
	}
	public function imp($id)
	{
		if(isset($this->imp[$id])) echo $this->imp[$id];
		return $this;
	}
	public function js()
	{
		echo $this->js;
		$this->jsLinha();
		return $this;
	}
	public function jsLinha()
	{
		if($this->js_linha) echo "\t\t<script type=\"text/javascript\">{$this->js_linha}</script>\n";
		return $this;
	}
	public function obter($id)
	{
		if(isset($this->imp[$id])) return $this->imp[$id];
		return null;
	}
	public function palavrasChave()
	{
		if($this->palavras) echo "\t\t<meta name=\"keywords\" content=\"{$this->palavras}\">\n";
		return $this;
	}
	public function redesSociais()
	{
		if($this->redes) echo $this->redes;
		return $this;
	}
	public function titulo()
	{
		if($this->titulo) echo "\t\t<title>{$this->titulo}</title>\n";
		return $this;
	}
}
