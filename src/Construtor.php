<?php
/*
==================================
------------- CIEBIT -------------
==================================
*/
namespace Ciebit;

class Construtor
{
    private $css;
    private $css_comando;
    private $css_linha = [];
    private $css_variaveis;
    private $js;
    private $js_linha = [];
    private $variaveis;

    // Variaveis de Configuração direta do Layout
    private $blocos = [];
    private $layouts = [];

    // Variaveis do cabeçalho
    private $descricao;
    private $palavras;
    private $titulo;

    // Open Graph do Facebook
    private $fb_og_descricao;
    private $fb_og_id;
    private $fb_og_img_url;
    private $fb_og_nome_site;
    private $fb_og_tipo;
    private $fb_og_titulo;
    private $fb_og_url;

    /**
     * Configura o objeto basedo em constantes de configuração do cb
     */
    public function __construct()
    {
        if (defined('CB_FB_ID')) {
            $this->fb_og_id = CB_FB_ID;
        }
        if (defined('CB_FB_NOME_SITE')) {
            $this->fb_og_nome_site = CB_FB_NOME_SITE;
        }
        if (defined('CB_FB_IMG_URL')) {
            $this->fb_og_img_url = CB_FB_IMG_URL;
        }
        if (defined('CB_FB_TIPO')) {
            $this->fb_og_tipo = CB_FB_TIPO;
        }
    }

    /**
     * Define um layout a ser chamado em um trecho html
     */
    public function bloco(string $nome, string $layout, $dados): self
    {
        $this->blocos[ $nome ] = array( $layout, $dados );
        return $this;
    }

    /**
     * Recebe o caminho de um arquivo css e junta com os demais
     */
    public function css(string $arquivo, string $grupo = 'cb_padrao' ): self
    {
        // Bloqueio para evitar duplicação
        if(
            isset($this->css[$grupo]) &&
            in_array($arquivo, $this->css[$grupo])
        ) {
            return $this;
        }

        $this->css[$grupo][] = $arquivo;
        return $this;
    }

    /**
     * Recebe um comando CSS
     */
    public function cssComando(string $comando): self
    {
        $this->css_comando.= $comando;
        return $this;
    }

    /**
     * Recebe o caminho de um arquivo css e junta com os demais
     */
    public function cssLinha(string $arquivo): self
    {
        $this->css_linha[] = $arquivo;
        return $this;
    }

    public function cssLinhaExtrair(): string
    {
        $codigo = '';
        foreach ($this->css_linha as $arquivo) {
            $codigo.= file_get_contents($arquivo);
        }
        return $codigo;
    }

    /**
     * Cria um novo arquivo CSS com a mesclagem de um grupo
     */
    public function cssMesclar(
        string $grupo,
        $caminho = null,
        string $url = ''
    ): self {
        $this->mesclar(
            $this->css,
            $grupo,
            'css',
            $caminho,
            $url,
            $this->css_variaveis['comandos'],
            $this->css_variaveis['valores']
        );
        return $this;
    }

    public function cssVar(string $nome, string $valor): self
    {
        $this->css_variaveis['chaves'][] = $nome;
        $this->css_variaveis['comandos'][] = "var(--{$nome})";
        $this->css_variaveis['valores'][] = $valor;

        return $this;
    }

    /**
     * Define o descrição página
     */
    public function descricao(string $descricao): self
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * Define um valor no Open Graph do Facebook
     */
    public function fbId(int $valor): self
    {
        //Definindo valor
        $this->fb_og_id = $valor;

        return $this;
    }

    /**
     * Pega o título da página, descrição e url e coloca no open graph
     */
    public function fbCopiar(): self
    {
        $this->fb_og_titulo = $this->titulo;
        $this->fb_og_descricao = $this->descricao;
        $this->fb_og_url = $_SERVER['REQUEST_URI'];

        return $this;
    }

    /**
     * Recebe o nome de uma variavel e valor e junta com os demais
     */
    public function imp(string $var, $val): self
    {
        $this->variaveis[$var] = $val;
        return $this;
    }

    public function impCss()
    {
        echo $this->cssHTML();
    }

    public function impJs()
    {
        echo $this->jsHTML();
    }

    /**
     * Recebe o caminho de um determinado arquivo
     */
    public function layout(string $arquivo): self
    {
        $this->layouts[] = $arquivo;
        return $this;
    }

    /**
     * Recebe o caminho de um arquivo javascript e junta com os demais
     */
    public function js(string $arquivo, $grupo = 'cb_padrao'): self
    {
        // Bloqueio para evitar duplicação
        if(
            isset($this->js[$grupo]) &&
            in_array($arquivo, $this->js[$grupo])
        ) {
            return $this;
        }

        $this->js[$grupo][] = $arquivo;
        return $this;
    }


    /**
     * Recebe o caminho de um arquivo js e junta com os demais
     */
    public function jsLinha(string $arquivo): self
    {
        $this->js_linha[] = $arquivo;
        return $this;
    }

    public function jsLinhaExtrair(): string
    {
        $codigo = '';
        foreach ($this->js_linha as $arquivo) {
            $codigo.= file_get_contents($arquivo);
        }
        return $codigo;
    }


    /**
     * Cria um novo arquivo JS com a mesclagem de um grupo
     */
    public function jsMesclar(string $grupo, $caminho = null, string $url = ''): self
    {
        $this->mesclar($this->js, $grupo, 'js', $caminho, $url);
        return $this;
    }

    /**
     * Cria um novo arquivo JS com a mesclagem de um grupo
     */
    public function scriptMesclar(string $grupo, $caminho = null, string $url = ''): self
    {
        return $this->jsMesclar($grupo, $caminho, $url);
    }


    /**
    * Constroe e Página e cria variaveis para utilização nos layouts
    **/
    public function construir()
    {
        $CT = new CTInterface;
        $CT->css = $CTcss = $this->cssHTML();
        $CT->css_linha = $this->cssLinhaExtrair();
        $CT->css_comando = $this->css_comando;
        $CT->js = $CTscript = $this->scriptHTML();
        $CT->js_linha = $this->jsLinhaExtrair();
        $CT->redes = $CTfacebook = $this->facebookHTML();
        $CT->titulo = $this->titulo;
        $CT->palavras = $this->palavras;
        $CT->descricao = $this->descricao;
        $CT->imp = $this->variaveis;
        $CT->blocos = $this->blocos;

        //Verificando se existe layouts
        foreach($this->layouts as $temp){
            require $temp;
        }
    }

    /**
     * Define as palavras chaves de uma página
     */
    public function palavrasChave(string $palavras): self
    {
        $this->palavras = $palavras;
        return $this;
    }

    /**
     * Define o título da página
     */
    public function titulo(string $titulo): self
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * Constroe um tecro html de chamada de css
     */
    private function cssHTML():string
    {
        $html = '';

        // Verifica se é válido está vazio
        if(!is_array($this->css)) return '';

        // Percorrendo grupos
        foreach($this->css as $grupo)
        {
            // Se não for um array pular
            if(!is_array($grupo)) continue;

            // Percorrendo arquivos
            foreach ($grupo as $arquivo)
            {
                $html.= "\t\t"."<link type=\"text/css\" rel=\"stylesheet\" href=\"{$arquivo}\">"."\n";
            }
        }

        return $html;
    }

    /**
    * Constroe um tecro html de informações para o Facebook
    **/
    private function facebookHTML():string
    {
        $ret = null;
        $url = null;

        //Verificando id
        if(isset($this->fb_og_id))
        {
            $ret .= "\t\t"."<meta property=\"fb:admins\" content=\"{$this->fb_og_id}\">"."\n";
        }

        //Verificando tipo
        if(isset($this->fb_og_tipo))
        {
            $ret .= "\t\t"."<meta property=\"og:type\" content=\"{$this->fb_og_tipo}\">"."\n";
        }

        //Verificando Nome do site
        if(isset($this->fb_og_nome_site))
        {
            $ret.= "\t\t"."<meta property=\"og:site_name\" content=\"{$this->fb_og_nome_site}\">"."\n";
        }

        //Verificando URL
        if(isset($this->fb_og_url))
        {
            $url = $this->fb_og_url;
        } else {
            $url = 'http://'. $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        }
        $ret.= "\t\t"."<meta property=\"og:url\" content=\"{$url}\">"."\n";

        //Verificando Imagem
        if(isset($this->fb_og_img_url))
        {
            $ret.= "\t\t"."<meta property=\"og:image\" content=\"{$this->fb_og_img_url}\">"."\n";
        }

        //Verificando título
        if(!isset($this->fb_og_titulo))
        {
            $this->fb_og_titulo = $this->titulo;
        }
        $ret.= "\t\t"."<meta property=\"og:title\" content=\"{$this->fb_og_titulo}\">"."\n";

        //Verificando Descrição
        if(!isset($this->fb_og_descricao))
        {
            $this->fb_og_descricao = $this->descricao;
        }
        $ret.= "\t\t"."<meta property=\"og:description\" content=\"{$this->fb_og_descricao}\">"."\n";

        return $ret;
    }

    /**
     * Constroe um tecro html de chamada de datas
     */
    private function datasHTML(): string
    {
        $ret = '';
        $atual = setlocale(LC_TIME, 0);
        $padrao = '%a, %d %b %Y %H:%M:%S GMT';

        //Definindo lingua
        setlocale(LC_TIME, 'english','en_US');

        //Trabalhando as datas
        if($this->criacao != null){
            $ret.= "\t\t".'<meta http-equiv="date" content="'
            . strftime($padrao, strtotime($this->criacao))
            . '">'."\n";
        }
        if($this->alteracao != null){
            $ret.= "\t\t".'<meta http-equiv="last-modified" content="'
            . strftime($padrao, strtotime($this->alteracao))
            . '">'."\n";
        }
        if($this->expiracao != null){
            $ret.= "\t\t".'<meta http-equiv="last-modified" content="'
            . strftime($padrao, strtotime($this->expiracao))
            . '">'."\n";
        }

        //Reconfigurando
        setlocale(LC_TIME, $atual);

        return $ret;
    }

    /**
     * Constroe um tecro html de chamada de script
     */
    private function mesclar(
        &$lista,
        string $grupo,
        string $extensao,
        $caminho = null,
        string $url = '',
        $var_chaves = null,
        $var_valores = null
    ):bool {
        $codigos  = '';
        $tmp_nome = '';

        // Encerra se o grupo não existir
        if( !isset($lista[$grupo]) || !is_array($lista[$grupo]) ) return false;

        // Caso não haja url utilizar a do caminho
        if(!$url) $url = $caminho;

        // Verificando se o caminho é absoluto
        if($caminho && $caminho[0] == '/' && strpos( $caminho, CB_DIR_CONFIG ) === false )
        {
            $caminho = CB_DIR_CONFIG.substr($caminho, 1);
        }

        // Lendo todos os arquivos css
        foreach($lista[$grupo] as $tmp_caminho)
        {
            if(
                $tmp_caminho[0] == '/' &&
                strpos( $tmp_caminho, CB_DIR_CONFIG ) === false
            ) {
                $tmp_caminho = CB_DIR_CONFIG . substr($tmp_caminho, 1);
            }
            $arq_data = filemtime($tmp_caminho);
            $tmp_nome.= "{$tmp_caminho}.{$extensao}-{$arq_data},";
            $tmp_codigo = file_get_contents($tmp_caminho);
            if(!$tmp_codigo) return false;
            $codigos.= $tmp_codigo;
        }

        // Se houver variavies efetuar substituição
        if( $var_chaves && $var_valores )
        {
            $tmp = str_replace( $var_chaves, $var_valores, $codigos );
            $codigos = $tmp;
        }

        // Criando nome do arquivo
        $arq_nome = md5($tmp_nome);
        $arq_nome.= '.'. $extensao;

        // Alterar grupo e encerra se o arquivo já existir
        if( is_file($caminho.$arq_nome) )
        {
            $lista[$grupo] = array($url.$arq_nome);
            return true;
        }

        // Cria o arquivo
        $arquivo = fopen($caminho.$arq_nome, 'w+');
        if(!$arquivo) return false;
        fwrite($arquivo, $codigos."\n");
        fclose($arquivo);

        // Altera caminho css
        $lista[$grupo] = array($url.$arq_nome);

        return true;
    }

    /**
     * Constroe um tecro html de chamada de script
     */
    private function scriptHTML(): string
    {
        $html = '';
        // Verifica se é válido está vazio
        if(!is_array($this->js)) return '';

        // Percorrendo grupos
        foreach($this->js as $grupo)
        {
            // Se não for um array pular
            if(!is_array($grupo)) continue;

            // Percorrendo arquivos
            foreach ($grupo as $arquivo)
            {
                $html.= "\t\t<script type=\"text/javascript\" src=\"{$arquivo}\"></script>\n";
            }
        }

        return $html;
    }
}
?>
