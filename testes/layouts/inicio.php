<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <?php
            $CT
            ->titulo()
            ->descricao()
            ->palavrasChave()
            ->redesSociais()
            ->css()
            ->js();
        ?>
    </head>
    <body>
        <h1><?php $CT->imp('titulo-h1'); ?></h1>
        <p><?php $CT->imp('paragrafo'); ?></p>
    </body>
</html>
