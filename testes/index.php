<?php
require '../vendor/autoload.php';

$Construtor = new \Ciebit\Construtor;

$Construtor
->titulo('Título de Página')
->descricao('Descrição da Página')
->css('estilos/inicio.css')
->js('scripts/inicio.js')
->layout('layouts/inicio.php')
->imp('titulo-h1', 'Teste Título')
->imp('paragrafo', 'Teste Parágrafo')
->construir();
